//! Provides constants for namespaces.

pub const CLIENT: &'static str = "jabber:client";
pub const COMPONENT_ACCEPT: &'static str = "jabber:component:accept";
pub const STREAM: &'static str = "http://etherx.jabber.org/streams";
pub const TLS: &'static str = "urn:ietf:params:xml:ns:xmpp-tls";
pub const SASL: &'static str = "urn:ietf:params:xml:ns:xmpp-sasl";
pub const BIND: &'static str = "urn:ietf:params:xml:ns:xmpp-bind";
pub const STANZAS: &'static str = "urn:ietf:params:xml:ns:xmpp-stanzas";
pub const PING: &'static str = "urn:xmpp:ping";
